const express = require ("express");
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

let users =[
		{
			"username": "johndoe",
			"password": "johndoe1234"
		}
	];

app.get("/home", (request, response) => {
	response.send("welcome to home page");
});

app.get("/users", (request, response) => {
	response.send(users);
});


app.delete("/delete-user", (request, response)=>{
	for(let i=0; i < users.length; i++){
		if (request.body.username == users[i].username){

			message = `user ${request.body.username}'s password has been deleted`;
			break;

		} 
		else
		{
			message =`user does not exist`;
		}
	}
	response.send(message);
});


//tells our server listen to port
app.listen(port, () => console.log(`server running at port ${port}`));