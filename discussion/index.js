// use the "require" directive to load the express module
const express = require ("express");

//create  ab application express
const app = express();

//for our application server to run, we need a port to listen to
const port = 3000;

//allow your app to read json data
app.use(express.json());

//allow your app to read other data types
//{extended: true} - by applying, it allows us to receive information in other data types
app.use(express.urlencoded({extended: true}));


// routes
//express has mehods corresponding to each HTTP method

//GET
//this route expect to receive a GET request at URI "/greet"
app.get("/greet", (request, response) => {
	//response.send - sends a response bact to the client
	response.send("hello from the /greet endpoint");
});


//POST
app.post("/hello", (request, response)=>{
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});


//simple registration form

//mock database
let users =[];

app.post("/signup", (request, response)=>{

	if (request.body.username !== '' && request.body.password !==''){
		users.push(request.body);

		response.send(`user ${request.body.username} succesfully registerd!`);
	}
	else{
		response.send("please input both username and password");
	}

});


//change password
app.patch("/change-password", (request, response)=>{

	//create a variable to store the message to be sent back to the client 
	let message;

	//if username provided in the postman and the username of the current object in the loop is the same 
	for(let i=0; i < users.length; i++){
		if (request.body.username == users[i].username){
			//change the password
			users[i].password = request.body.password;

			//message to be sed back if password has been updateed successfully 
			message = `user ${request.body.username}'s password has been updated`;
			break;

		} //if no user match
		else
		{
			message =`user does not exist`;
		}
	}

	response.send(message);
});


















//tells our server listen to port
app.listen(port, () => console.log(`server running at port ${port}`));



































